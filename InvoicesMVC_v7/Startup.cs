﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InvoicesMVC_v7.Startup))]
namespace InvoicesMVC_v7
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
