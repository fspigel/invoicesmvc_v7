﻿using InvoicesMVC_v7.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace InvoicesMVC_v7.Controllers
{
    [Authorize]
    public class InvoicesController : Controller
    {
        private InvoicesMVC_v71Context db = new InvoicesMVC_v71Context();

        // GET: Invoices
        public ActionResult Index(string receiverName, int? id, DateTime? minDateCr, DateTime? maxDateCR, DateTime? minDateDue, DateTime? maxDateDue)
        {
            ViewBag.Title = "Invoices.com - Invoice table";

            //TODO: investigate why cost searching doesn't work
            var invoices = from inv in db.Invoices
                           select inv;

            if (!string.IsNullOrEmpty(receiverName))
            {
                invoices = invoices.Where(s => s.ReceiverName.Contains(receiverName));
            }

            if (id != null)
            {
                string idstring = id.ToString();
                invoices = invoices.Where(s => s.InvoiceID.ToString().Contains(idstring));
            }

            if(minDateCr != null)
            {
                invoices = invoices.Where(s => s.CreationDate > minDateCr);
            }

            if (maxDateCR != null)
            {
                invoices = invoices.Where(s => s.CreationDate < minDateCr);
            }

            if (minDateDue != null)
            {
                invoices = invoices.Where(s => s.DeadlineDate > minDateCr);
            }

            if (maxDateCR != null)
            {
                invoices = invoices.Where(s => s.DeadlineDate > minDateCr);
            }


            return View(invoices);
        }

        // GET: Invoices/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // GET: Invoices/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Invoices/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "InvoiceID,DeadlineDate,ReceiverName,TaxCountry")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                invoice.CreationDate = DateTime.Now;
                db.Invoices.Add(invoice);
                db.SaveChanges();
                
                return RedirectToAction("Index");
            }

            return View(invoice);
        }

        /// <summary>
        /// Adds some sample purchases to an invoice
        /// </summary>
        /// <param name="invoice"></param>
        public void PopulatePurchases(Invoice invoice)
        {
            Purchase p1 = new Purchase("Apples", 1.99f, 5);
            Purchase p2 = new Purchase("Oranges", 8.99f, 3);
            invoice.Purchases.Add(p1);
            invoice.Purchases.Add(p2);
        }

        // GET: Invoices/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "InvoiceID,CreationDate,DeadlineDate,ReceiverName,TotalCost,AuthorName,AuthorID")] Invoice invoice)
        {
            if (ModelState.IsValid)
            {
                db.Entry(invoice).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(invoice);
        }

        // GET: Invoices/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Invoice invoice = db.Invoices.Find(id);
            if (invoice == null)
            {
                return HttpNotFound();
            }
            return View(invoice);
        }

        // POST: Invoices/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Invoice invoice = db.Invoices.Find(id);
            //Cascaded delete
            for(int i=0; i<invoice.Purchases.Count; i++)
            {
                db.Purchases.Remove(invoice.Purchases[i]);
            }
            db.Invoices.Remove(invoice);
            db.SaveChanges();
            return RedirectToAction("Index");
            
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
