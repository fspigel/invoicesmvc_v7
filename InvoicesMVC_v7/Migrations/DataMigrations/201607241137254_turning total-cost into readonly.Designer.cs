// <auto-generated />
namespace InvoicesMVC_v7.Migrations.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class turningtotalcostintoreadonly : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(turningtotalcostintoreadonly));
        
        string IMigrationMetadata.Id
        {
            get { return "201607241137254_turning total-cost into readonly"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
