namespace InvoicesMVC_v7.Migrations.DataMigrations
{
    using Models;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<InvoicesMVC_v7.Models.InvoicesMVC_v71Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\DataMigrations";
        }

        protected override void Seed(InvoicesMVC_v71Context context)
        {
            //Cascaded delete
            foreach (Invoice invoice in context.Invoices)
            {
                for (int i = 0; i < invoice.Purchases.Count; i++)
                {
                    context.Purchases.Remove(invoice.Purchases[i]);
                }
                context.Invoices.Remove(invoice);
            }


            InvoicesMVC_v71Context db = new InvoicesMVC_v71Context();

            var Dates = new List<DateTime>
            {
                new DateTime(1999, 1, 1),
                new DateTime(1999, 5, 6),
                new DateTime(2000, 3, 12),
                new DateTime(2001, 4, 29),

                new DateTime(2017, 5, 4),
                new DateTime(2017, 11, 11),
                new DateTime(2018, 3, 5),
                new DateTime(2018, 12, 31)
            };

            var Purchases = new List<Purchase>
            {
                new Purchase("Apples", 5, 2),
                new Purchase("Oranges", 3, 1),

                new Purchase("Video game", 60, 1),
                new Purchase("Game console", 599.99f, 1),
                new Purchase("Video game", 60, 2),

                new Purchase("Tesla Model S", 71000, 1),
                new Purchase("Video game", 60, 1),
                new Purchase("TV set", 4500, 1),

                new Purchase("A part of northern Canada", 3.99f, 1),
                new Purchase("A small nearby planetoid", 8000000.99f, 1),
                new Purchase("Literally the Moon", 4500000.00f, 1),

                new Purchase("A social life", 50, 1),
                new Purchase("2 dollars", 1, 1),
                new Purchase("2 dollars", 1, 500)
            };

            Purchases.ForEach(s => context.Purchases.AddOrUpdate(p => p.PurchaseID, s));
            context.SaveChanges();

            string sampleUserID = "758a0577-af32-49b2-92ff-f7ae8e8a254c";

            var Invoices = new List<Invoice>
            {
                new Invoice(Dates[0], Dates[4], "George", sampleUserID, "Countrystan"),
                new Invoice(Dates[1], Dates[5], "Billy", sampleUserID, "Countrystan"),
                new Invoice(Dates[2], Dates[6], "Car guy", sampleUserID, "Taxland"),
                new Invoice(Dates[3], Dates[7], "Evil genius", sampleUserID, "Sovereign nation of Dr. Destructo"),
                new Invoice(Dates[3], Dates[7], "Fred", sampleUserID, "Countrystan")
            };

            var invoiceTemp = Invoices[0];

            var purchaseListTemp = invoiceTemp.Purchases;

            var purchaseTemp = Purchases[0];

            purchaseListTemp.Add(purchaseTemp);

            Invoices[0].Purchases.Add(Purchases[0]);
            Invoices[0].Purchases.Add(Purchases[1]);
            Invoices[1].Purchases.Add(Purchases[2]);
            Invoices[1].Purchases.Add(Purchases[3]);
            Invoices[1].Purchases.Add(Purchases[4]);
            Invoices[2].Purchases.Add(Purchases[5]);
            Invoices[2].Purchases.Add(Purchases[6]);
            Invoices[2].Purchases.Add(Purchases[7]);
            Invoices[3].Purchases.Add(Purchases[8]);
            Invoices[3].Purchases.Add(Purchases[9]);
            Invoices[3].Purchases.Add(Purchases[10]);
            Invoices[4].Purchases.Add(Purchases[11]);
            Invoices[4].Purchases.Add(Purchases[12]);
            Invoices[4].Purchases.Add(Purchases[13]);

            Invoices.ForEach(s => context.Invoices.AddOrUpdate(p => p.InvoiceID, s));
            context.SaveChanges();
        }


    }
}
