namespace InvoicesMVC_v7.Migrations.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class turningtotalcostintoreadonly : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Invoices", "TotalCost");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Invoices", "TotalCost", c => c.Single(nullable: false));
        }
    }
}
