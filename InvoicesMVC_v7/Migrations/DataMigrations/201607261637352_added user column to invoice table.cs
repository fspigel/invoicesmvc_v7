namespace InvoicesMVC_v7.Migrations.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedusercolumntoinvoicetable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoices", "AuthorID", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoices", "AuthorID");
        }
    }
}
