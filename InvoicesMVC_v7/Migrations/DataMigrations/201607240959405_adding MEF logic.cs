namespace InvoicesMVC_v7.Migrations.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingMEFlogic : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Invoices", "TaxCountry", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Invoices", "TaxCountry");
        }
    }
}
