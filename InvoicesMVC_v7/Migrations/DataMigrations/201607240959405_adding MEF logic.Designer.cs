// <auto-generated />
namespace InvoicesMVC_v7.Migrations.DataMigrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addingMEFlogic : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addingMEFlogic));
        
        string IMigrationMetadata.Id
        {
            get { return "201607240959405_adding MEF logic"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
