namespace InvoicesMVC_v7.Migrations.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceID = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        DeadlineDate = c.DateTime(nullable: false),
                        ReceiverName = c.String(),
                        TotalCost = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.InvoiceID);
            
            CreateTable(
                "dbo.Purchases",
                c => new
                    {
                        PurchaseID = c.Int(nullable: false, identity: true),
                        ProductDescription = c.String(),
                        SinglePrice = c.Single(nullable: false),
                        Amount = c.Single(nullable: false),
                        Invoice_InvoiceID = c.Int(),
                    })
                .PrimaryKey(t => t.PurchaseID)
                .ForeignKey("dbo.Invoices", t => t.Invoice_InvoiceID)
                .Index(t => t.Invoice_InvoiceID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        SinglePrice = c.Single(nullable: false),
                        Purchase_PurchaseID = c.Int(),
                    })
                .PrimaryKey(t => t.ProductID)
                .ForeignKey("dbo.Purchases", t => t.Purchase_PurchaseID)
                .Index(t => t.Purchase_PurchaseID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Products", "Purchase_PurchaseID", "dbo.Purchases");
            DropForeignKey("dbo.Purchases", "Invoice_InvoiceID", "dbo.Invoices");
            DropIndex("dbo.Products", new[] { "Purchase_PurchaseID" });
            DropIndex("dbo.Purchases", new[] { "Invoice_InvoiceID" });
            DropTable("dbo.Products");
            DropTable("dbo.Purchases");
            DropTable("dbo.Invoices");
        }
    }
}
