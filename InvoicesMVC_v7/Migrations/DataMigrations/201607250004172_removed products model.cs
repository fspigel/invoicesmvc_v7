namespace InvoicesMVC_v7.Migrations.DataMigrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedproductsmodel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Products", "Purchase_PurchaseID", "dbo.Purchases");
            DropIndex("dbo.Products", new[] { "Purchase_PurchaseID" });
            AlterColumn("dbo.Purchases", "ProductDescription", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        SinglePrice = c.Single(nullable: false),
                        Purchase_PurchaseID = c.Int(),
                    })
                .PrimaryKey(t => t.ProductID);
            
            AlterColumn("dbo.Purchases", "ProductDescription", c => c.String());
            CreateIndex("dbo.Products", "Purchase_PurchaseID");
            AddForeignKey("dbo.Products", "Purchase_PurchaseID", "dbo.Purchases", "PurchaseID");
        }
    }
}
