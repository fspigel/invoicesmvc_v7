﻿//namespace InvoicesMVC_v7.Migrations
//{
//    using Microsoft.AspNet.Identity;
//    using Microsoft.AspNet.Identity.EntityFramework;
//    using Models;
//    using System;
//    using System.Collections.Generic;
//    using System.Data.Entity;
//    using System.Data.Entity.Migrations;
//    using System.Linq;

//    internal sealed class Configuration : DbMigrationsConfiguration<InvoicesMVC_v7.Models.InvoicesMVC_v71Context>
//    {
//        public Configuration()
//        {
//            AutomaticMigrationsEnabled = false;
//            ContextKey = "InvoicesMVC_v7.Models.InvoicesMVC_v71Context";
//        }

//        bool AddUserAndRole(ApplicationDbContext context)
//        {
//            IdentityResult ir;
//            var rm = new RoleManager<IdentityRole>
//                (new RoleStore<IdentityRole>(context));
//            ir = rm.Create(new IdentityRole("canEdit"));
//            var um = new UserManager<ApplicationUser>(
//                new UserStore<ApplicationUser>(context));
//            var user = new ApplicationUser()
//            {
//                UserName = "user1@contoso.com",
//            };
//            ir = um.Create(user, "P_assw0rd1");
//            if (ir.Succeeded == false)
//                return ir.Succeeded;
//            ir = um.AddToRole(user.Id, "canEdit");
//            return ir.Succeeded;
//        }

//        protected override void Seed(InvoicesMVC_v7.Models.InvoicesMVC_v71Context context)
//        {
//            //AddUserAndRole(context);


//            ////Cascaded delete
//            //foreach(Invoice invoice in context.Invoices)
//            //{
//            //    for(int i=0; i<invoice.Purchases.Count; i++)
//            //    {
//            //        context.Purchases.Remove(invoice.Purchases[i]);
//            //    }
//            //    context.Invoices.Remove(invoice);
//            //}

//            //foreach(Purchase purchase in context.Purchases)
//            //{
//            //    context.Purchases.Remove(purchase);
//            //}

//            //    var Purchases = new List<Purchase>
//            //{
//            //    new Purchase("Apples", 1.99f, 5),
//            //    new Purchase("Apples", 1.99f, 1),
//            //    new Purchase("Apples", 1.99f, 17),
//            //    new Purchase("Oranges", 7.99f, 3),
//            //    new Purchase("Oranges", 7.99f, 4),
//            //    new Purchase("MSI GTX 1080 GPU", 699.99f, 2),
//            //    new Purchase("MSI GTX 1080 GPU", 699.99f, 1),
//            //    new Purchase("MSI GTX 1080 GPU", 699.99f, 1),
//            //    new Purchase("A small nearby planetoid", 698.99f, 1),
//            //    new Purchase("2 dollars", 1, 1),
//            //    new Purchase("2 dollars", 1, 500)
//            //};
//            //Purchases.ForEach(s => context.Purchases.AddOrUpdate(p => p.PurchaseID, s));
//            //context.SaveChanges();

//            //var Dates = new List<DateTime>
//            //{
//            //    new DateTime(1997, 4, 5),
//            //    new DateTime(1997, 5, 5),
//            //    new DateTime(1998, 2, 2),
//            //    new DateTime(1999, 12, 31)
//            //};

//            //var Invoices = new List<Invoice>
//            //{
//            //    new Invoice(Dates[0], Dates[1], "Frank", "Mark", "markyboy123"),
//            //    new Invoice(Dates[0], Dates[2], "Amy", "Mark", "markyboy123"),
//            //    new Invoice(Dates[1], Dates[2], "Frank", "Amy", "NotARealID"),
//            //    new Invoice(Dates[2], Dates[3], "Bob", "Frank", "FrankTheMank"),
//            //};

//            //Invoices[0].Purchases.Add(Purchases[0]);
//            //Invoices[0].Purchases.Add(Purchases[1]);
//            //Invoices[0].Purchases.Add(Purchases[2]);
//            //Invoices[1].Purchases.Add(Purchases[3]);
//            //Invoices[1].Purchases.Add(Purchases[4]);
//            //Invoices[1].Purchases.Add(Purchases[5]);
//            //Invoices[2].Purchases.Add(Purchases[6]);
//            //Invoices[2].Purchases.Add(Purchases[7]);
//            //Invoices[2].Purchases.Add(Purchases[8]);
//            //Invoices[3].Purchases.Add(Purchases[9]);
//            //Invoices[3].Purchases.Add(Purchases[10]);

//            //foreach (Invoice invoice in Invoices) invoice.SetTotalCost();

//            //Invoices.ForEach(s => context.Invoices.AddOrUpdate(p => p.InvoiceID, s));
//            //context.SaveChanges();
//        }
//    }
//}
