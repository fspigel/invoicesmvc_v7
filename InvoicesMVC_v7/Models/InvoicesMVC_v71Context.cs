﻿using System.Data.Entity;

namespace InvoicesMVC_v7.Models
{
    public class InvoicesMVC_v71Context : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public InvoicesMVC_v71Context() : base("name=InvoicesMVC_v71Context")
        {
        }

        public System.Data.Entity.DbSet<InvoicesMVC_v7.Models.Invoice> Invoices { get; set; }

        public System.Data.Entity.DbSet<InvoicesMVC_v7.Models.Purchase> Purchases { get; set; }
    }
}
