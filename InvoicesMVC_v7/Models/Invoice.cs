﻿using InvoicesMVC_v7.Taxes;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;
using System.Web.Mvc;

namespace InvoicesMVC_v7.Models
{
    public class Invoice
    {
        //Primary Key
        public int InvoiceID { get; set; }

        //Misc. info
        [DisplayName("Created on")]
        public DateTime CreationDate { get; set; }
        [DisplayName("Due by")]
        public DateTime DeadlineDate { get; set; }
        [DisplayName("Recepient")]
        public string ReceiverName { get; set; }

        //Order details
        public virtual List<Purchase> Purchases { get; set; }

        //Auto-calculated property
        [DisplayName("Total Cost (before taxes)")]
        [DataType(DataType.Currency)]
        //public float TotalCost { get; set; }
        public float TotalCost
        {
            get
            {
                float f = 0;
                foreach (Purchase purchase in Purchases) { f += purchase.TotalPrice; }
                return f;
            }
        }

        //Taxing logic
        [ImportMany]
        IEnumerable<Lazy<ITax, ITaxData>> taxes;
        public string TaxCountry { get; set; }
        [DisplayName("Total Cost (after taxes)")]
        [DataType(DataType.Currency)]
        public float PriceWithTax
        {
            get
            {
                foreach (Lazy<ITax, ITaxData> tax in taxes)
                {
                    if (tax.Metadata.Country == TaxCountry) return tax.Value.ApplyTax(TotalCost);
                }
                return TotalCost;
            }
        }
        private CompositionContainer container;
        public List<SelectListItem> GetAvailableCountries()
        {
            List<SelectListItem> sList = new List<SelectListItem>();
            foreach (Lazy<ITax, ITaxData> tax in taxes)
            {
                sList.Add(new SelectListItem
                {
                    Text = tax.Metadata.Country,
                    Value = tax.Metadata.Country
                });
            }
            return sList;
        }

        //Invoice author info
        [DisplayName("Author")]
        public string AuthorName { get { return Author.DisplayName; } }
        public string AuthorID { get; set; }
        public ApplicationUser Author {
            get
            {
                return AppUserById(AuthorID);
            }
                
        }

        private ApplicationUser AppUserById(string id)
        {
            ApplicationDbContext db = new ApplicationDbContext();
            foreach(ApplicationUser user in db.Users)
            {
                if (user.Id == id) return user;
            }
            return null;
        }

        public Invoice()
        {
            Purchases = new List<Purchase>();
            if (HttpContext.Current != null)
            {
                AuthorID = HttpContext.Current.User.Identity.GetUserId();
            }
            //else Author = ApplicationUser.SampleUser;
            else AuthorID = "";

            //MEF
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Invoice).Assembly));

            container = new CompositionContainer(catalog);
            container.ComposeParts(this);
        }

        public Invoice(DateTime creationDate, DateTime deadLine, string receiver, string authorID, string taxCountry) : this()
        {
            CreationDate = creationDate;
            DeadlineDate = deadLine;
            ReceiverName = receiver;
            AuthorID = authorID;
            TaxCountry = taxCountry;
        }

        public ApplicationUser GetInvoiceAuthor()
        {
            return Author;
        }

    }
}