﻿using System.ComponentModel.DataAnnotations;

namespace InvoicesMVC_v7.Models
{
    public class Purchase
    {
        public int PurchaseID { get; set; }

        [Required]
        public string ProductDescription { get; set; }

        [Required]
        public float SinglePrice { get; set; }

        [Required]
        public float Amount { get; set; }

        [DataType(DataType.Currency)]
        public float TotalPrice { get { return Amount * SinglePrice; } }

        public Purchase() { }

        public Purchase(string description, float singleprice, float amount)
        {
            ProductDescription = description;
            SinglePrice = singleprice;
            Amount = amount;
        }
    }

}