﻿using System.ComponentModel.Composition;

namespace InvoicesMVC_v7.Taxes
{
    [Export(typeof(ITax))]
    [ExportMetadata("Country", "Countrystan")]
    public class CountrystanTax : ITax
    {
        public float ApplyTax(float originalPrice) { return originalPrice * 1.2f; }
    }

    [Export(typeof(ITax))]
    [ExportMetadata("Country", "Taxland")]
    public class Taxland : ITax
    {
        public float ApplyTax(float originalPrice) { return originalPrice * 1.9f; }
    }

    [Export(typeof(ITax))]
    [ExportMetadata("Country", "Sovereign nation of Dr. Destructo")]
    public class SNDD : ITax
    {
        public float ApplyTax(float originalPrice) { return originalPrice * 1.01f; }
    }
}