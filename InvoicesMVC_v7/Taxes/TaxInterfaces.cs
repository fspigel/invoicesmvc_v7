﻿namespace InvoicesMVC_v7.Taxes
{
    public interface ITax
    {
        float ApplyTax(float originalPrice);
    }

    public interface ITaxData
    {
        string Country { get; }
    }
}