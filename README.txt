Ovo je gotova aplikacija za Invoices.com - najbolje mjesto za pregledavanje faktura na va�em localhostu

DATABAZE/CODE-FIRST:
Po svemu sudeci trebali bi dobiti moje baze podataka pred-generirane sa hrpom testnih unosa.
Ako nisu uploadane na git, mo�ete generirati testne podatke sa Configuration.Seed() metodom. 
Potrebno je u Package Manager Console pokrenuti sljedecu naredbu:

Update-Database -ConfigurationTypeName InvoicesMVC_v7.Migrations.DataMigrations.Configuration

migracije su uspostavljene za fakture i za bazu podataka o korisnicima. Relevantne naredbe za njih se nalaze
u MigrationsCopyPasta.txt

MEF:
Oporezivanje preko MEFa je implementirano. Za dodavanje novih poreza potrebno je dodati novu klasu bilo gdje
u assemblyu (najbolje u folder Taxes). Pogledati SampleTaxes.cs za primjer.

PRETRA�IVANJE:
Ja sam ponekad jedna prilicno zbunjena osoba, pa sam iz "Potrebno je razviti sustav koji ce omoguciti 
korisnicima stvaranje i pregledavanje faktura." shvatio da je potrebno dodati pretra�ivanje faktura preko
search barova. Kasnije mi je jasno receno, "fakture se ne trebaju moci pretra�ivati." (Marin Glibic),
ali to sam takoder uspio krivo shvatiti. Uglavnom podr�ka za pretra�ivanje je isto implementirana 
htjeli-ne htjeli. 

UNO�ENJE NOVIH PODATAKA U APLIKACIJU:
Potrebno je prvo stvoriti novu fakturu, zatim zapamniti njen InvoiceID. Nakon toga otici na indeks za
kupovine (~/Purchases1) te unjeti nove kupovine u bazu. Pri uno�enju treba odabrati InvoiceID od maloprije 
stvorene fakture.

STOCK USER:
Ako ne �elite stvarati novi account, iskoristite ovaj:
Username:
haha@fake.email
Password:
N$h*6h^gjt4T7J0&V$Ss

(MVC je po defaultu rigorozan sa zahtjevima za kvalitetu passworda)